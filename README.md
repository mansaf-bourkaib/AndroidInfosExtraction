# Android Infos Extraction

Ce projet appartient au centre de recherche SnT de l'université de Luxembourg, il permet d'extraire des informations sur des applications Android qui se trouvent dans leur base de données Androzoo. Ce projet est écrit en Python3 et il utilise des services comme Redis et Androzoo.

## Getting started
Pour utiliser ce projet il faut suivre les étapes suivantes:

### Installer les dépendances 

Installer les dépendances python3 en utilisant le fichier Source/requirements.txt et la commande pip
# ![Alt text](./Images/c11.PNG?raw=true "Title")

### Remplir le fichier Environment.py
Il faut remplir le fichier Source/Environment.py avec toutes les informations nécessaires, comme les informations du serveur Redis, les informations sur l'API Androzoo et la clé d'API les informations sur le serveur MongoDB...

### Remplir la base de données Redis
La base de données redis va contenir les sha256 de toutes les applications qu'on veut récupérer leurs informations.Vous pouvez mettre manuellement ces sha256 comme vous pouvez utiliser le script Source/Redis_function.py qui permet de remplir votre base de données Redis avec les SHA256 d'une façon aléatoire.L'exemple suivant illustre l'utilisation du script Redis_function.py pour charger aléatoirement 1000 SHA256 dans la base de données redis.

# ![Alt text](./Images/c12.PNG?raw=true "Title")
### Démarrer le processus d'extraction
Après avoir rempli la base de données redis ,vous pouvez démarrer l'extraction d'informations en exécutent directement le fichier Source/main.py,Ce processus d'extraction tourne jusqu'à ce que la base de données redis devienne vide.Les résultats d'extraction sont stockés dans la base de données MongoDB.

**> python3 main.py**

## La structure des données extraites
La structure json des données extraites et la suivante:

{  
APK: {  'appID', 'appName',
        'sha256',
        'sha512',
        'md5',
        'codeVersion',
        'sdkVersion',
        'minSdk',
        'permissions',
        'third_party_permissions',
        'hardware_components',
        'activities',
        'services',
        'receivers',
        'providers',
        'libraries',
        'intentFilter',
        'mainActivity',
        'apk_sig_files',
        'apk_is_signed',
        'apk_is_valid' ,
        'files',
        'files_extension',
        'certificats'
}  

,DVM:{'header',
        'classes',
        'methods',
        'fields',
        'strings',
        'invokes',
        'branch_opcodes',
        'apiVersion',
        'len_methods',
        'is_debug',
        'urls',
        'suspicious_api',
        'external_classes',
        'load_methods',
        'load_classes',
        'icc_methods',
        'pending_intent',
        'reflection'}  

,DEX:{
        "dex_version",
        "dex_checksum",
        "dex_file_size",
        "dex_header_size",
        "dex_Magic",
        "dex_sig",
        "dex_endian_tag"
}  

,Virus_Total:{
        'vt_detection',
         'vt_scan_date'
}
}


Voir l'exemple [Json Output](./example/output.json)

## Les informations extraites

|Informations | Déscription        |
| :--------------- |:---------------:|
| APK.appID  |   L’ID d’application est utilisé pour identifier de manière unique une application sur un appareil et dans Google Play.        |
| APK.appName | Le nom de l’application. Exemple : Teams            |
| APK.sha256 | Le Hash SHA256 du fichier APK      |
| APK.sha512 | Le HASH SHA512 du fichier APK     |
| APK.md5 | Le HASH MD5 du fichier APK |
| APK.codeVersion | Le code de version est fourni aux développeurs d’applications pour nommer les différentes versions d’applications.|
| APK.sdkVersion| La version ou l’application a été ciblée pour fonctionner Idéalement. |
| APK.minSdk | La première version du SDK Android sur laquelle l’application peut s’exécuter.|
| APK.permissions | Les autorisations, déclarées par les développeurs d’applications. Exemple: BLUETOOTH. |
| APK.hardware_components | Liste des matériels physique utilisée par l'application, Ex: Caméra, Gps. |
| APK.activities | liste des activities |
| APK.services | liste des services |
| APK.receivers| liste des receivers  |
| APK.providers| liste des providers |
| APK.libraries | liste de libraries Externe  |
| APK.intentFilter | Liste d'actions et catégories de intentFilter existantes dans le fichier manifeste. |
| APK.mainActivity | Le nom du main Activity. |
| APK.apk_sig_files | La liste des fichiers de signature de l'application.|
| APK.apk_is_signed | Un booléen pour vérifier si l'application est signée. |
| APK.apk_is_valid | Un booléen pour vérifier si l'application est signée avec la bonne clé. |
| APK.files_extension | Liste des extensions de fichiers existantes dans le fichier APK. |
| APK.certificats | Liste des certificats existants dans l'application avec des informations sur l'autorité de certification, le sha252 du certificat, Algo de hachage, numéro de certificat.|
| DVM.classes | La liste des noms des classes existantes dans l'application. |
| DVM.methods | La liste des méthodes existantes dans l'application.|
| DVM.strings | La liste de toutes les chaînes de caractères existantes dans l’application.|
| DVM.invokes | Identifie chaque appel d’une fonction par une classe.|
| DVM.len_methods | Le nombre de méthodes utilisé dans l'application. |
| DVM.is_debug | Booléen qui indique si le mode debug est activé. |
| DVM.urls | Liste des urls sollicité par l'application.|
| DVM.suspicious_api | Liste des API suspicieuses utilisée par l'application.|
| DVM.external_classes | Liste des classes externe. |
| DVM.load_methods | La liste des méthodes Load utilisée dans l'application. |
| DVM.load_classes|  La liste des classes Load utilisée dans l'application. |
| DVM.icc_methods | Le nombre de méthodes ICC. |
| DVM.reflection | Un booléen sur l'utilisation de la réflexion par l'application. |
| DEX.dex_version | La verison du fichier DEX. |
| DEX.dex_checksum | Le checksum du fichier DEX.|
| DEX.dex_file_size | La taille du fichier DEX.|
| DEX.dex_header_size | La taille de l'entête DEX.|
| DEX.dex_sig | La signature du fichier DEX.|
| Virus_Total.vt_detection | Le score virusTotal.|
| Virus_Total.vt_scan_date | La date de scan de l'application.|







