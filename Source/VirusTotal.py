from virus_total_apis import PublicApi as VirusTotalPublicApi
import Environment

"""This module allows you to use Api Virus Total"""



def get_virus_total_score(sha256):

    vt = VirusTotalPublicApi(Environment.VIRUSTOTAL_APIKEY)
    response = vt.get_file_report(sha256)

    if response["response_code"] == 200:  # if we have response from virus Total
        virus_total_score = response['results']['positives']
    else:
        virus_total_score = -1
    return virus_total_score


def get_virus_total_infos(sha256):
    vt = VirusTotalPublicApi(Environment.VIRUSTOTAL_APIKEY)
    response = vt.get_file_report(sha256)
    if response["response_code"] == 200:
        info={}
        info["positives"]=response['results']['positives']
        info["total"]=response['results']['total']
        info["scan_date"]=response['results']['scan_date']
    return info



