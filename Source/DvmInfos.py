from androguard.core.bytecodes import dvm as AndroDVM
import base64
import re
import functions

"""This module makes it possible to extract the information from the DVM object"""

def is_debug(apkObj):
    """Check a DVM has a debug flag set."""
    return isinstance(apkObj.dvm.debug, AndroDVM.DebugInfoItem)

def xformat(apkObj):
    """Return the format of a DVM object."""
    return apkObj.dvm.get_format_type()


def header(apkObj):
    """Extract header information from an APK object."""
    header = apkObj.dvm.header

    return {
        "magic": header.magic,
        "checksum": str(header.checksum),
        "file_size": header.file_size,
        "signature": str(base64.b64encode(header.signature)),
    }

def classes(apkObj):
    """Extract classes information from an APK object."""
    class_list=apkObj.dx.get_classes()
    classes_info=[]
    for c in class_list:
        classes_info.append(c.name)
    return classes_info

def external_classes(apkObj):
    """Extract external classes information from an APK object."""
    ex_classes_list = apkObj.dx.get_external_classes()
    ex_classes_name=[]

    for c in ex_classes_list:
        ex_classes_name.append(c.name)

    return list(set(ex_classes_name))


def methods(apkObj):
    """Extract methods information from an APK object."""
    class_list = classes(apkObj)
    methods_list = list()
    for cls in  class_list:
        for method in apkObj.dx.classes[cls].get_methods():
            methods_list.append(method.name)
    return methods_list



def fields(apkObj):
    """Extract fields information from an APK object."""
    fields_info = []
    for cls in apkObj.dvm.classes.class_def:
        for f in cls.get_fields():
            info={}
            info["name"]=f.get_name()
            info["class_name"]=f.get_class_name()
            info["access_flag"]=f.get_access_flags()
            info["value"]=f.get_init_value().get_value() if f.init_value else None
            fields_info.append(info)
    return fields_info

def strings(apkObj):
    """Extract Strings from an APK object."""
    strings_list=apkObj.dvm.get_strings()
    return list(set(strings_list))


def invokes(apkObj):
    """Extract invokes information from an APK object."""
    invoke = list()

    # many nested levels ...
    for code in apkObj.dvm.codes.code:
        for ins in code.code.get_instructions():
            try:
                if ins.get_kind() == AndroDVM.KIND_METH:
                    invoke.append(ins.get_translated_kind())
            except Exception:
                pass
    return  invoke

def get_urls(apkObj):
    """Extract Urls from an APK object."""
    url_regex = r"(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))"
    strings_list = apkObj.dvm.get_strings()
    urls = list()
    for elem in strings_list:
        url = re.findall(url_regex, elem)
        if bool(url) == True:
            urls.append(url[0][0])
    return list(set(urls))

def get_reflected_api(apkObj):
    """Extract Reflected api used in an APK object."""

    inv=invokes(apkObj)
    for elem in inv:
        if elem =="Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;" or elem=="Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;":
            return True



def get_suspicious_api_calls(apkObj):
    """find out if an apk contains suspicious api"""
    suspicious_api_calls = ['getSimSerialNumber', 'getSubscriberId', 'getImei', 'getDeviceId', 'getLineNumber',
                            'getNetworkOperator', 'newOutgoingCalls'
                                                  'sendTextMessage', 'sendDataMessage', 'HttpClient.execute',
                            'getOutputStream', 'getInputStream', 'getNetworkInfo'
                                                                 'openConnection', 'httpClient.execute',
                            'getInstalledPackages', 'startService', 'setComponent', 'getWifiState'
                                                                                    'getEnabled', 'setWifiEnabled',
                            'URL', 'getAssets', 'openFileOutput', 'exec', 'DexClassLoader', 'Cipher.getInstance','getCallState',
                            'getDataActivity','getLine1number','getNetworkType','getSimOperator','getSimState','getActiveNetworkInfo',
                            'getConnectionInfo','getSupplicantState']
    suspicious_api_list = list()
    method_list=methods(apkObj)

    for elem in method_list:
        if elem in suspicious_api_calls:
            suspicious_api_list.append(elem)

    return list(set(suspicious_api_list))

def get_load_methods(apkObj):
    """find out if an apk contains Load method for dynamic code"""
    load_methods = ['System.loadLibrary', 'System.load', 'Runtime.load', 'Runtime.loadLibrary', 'loadLibrary','DexClassLoader','load']
    load_methods_list = list()

    method_list = methods(apkObj)

    for elem in method_list:
        if elem in load_methods:
            load_methods_list.append(elem)


    return list(set(load_methods_list))

def get_load_classes(apkObj):
    """find out if an apk contains class for dynamic code"""
    load_classes=["Ldalvik/system/PathClassLoader;","Ldalvik/system/DexClassLoader;"]
    load_classes_list = list()

    cls = classes(apkObj)

    for clas in cls:
        if clas in load_classes:
            load_classes_list.append(clas)
    return list(set(load_classes_list))




def get_icc_methods(apkObj):
    """find all icc methods used in an apk object"""
    icc_methods_list=["startActivity","startService","sendBroadcast"]
    method=methods(apkObj)

    icc_methods_used=list()
    total_icc=0

    for elem in method:
        if elem in icc_methods_list:
            icc_methods_used.append(elem)
            total_icc+=1
    icc_methods_used=set(icc_methods_used) #delete duplicate
    info={"methods":list(set(icc_methods_used)),"total":total_icc}

    return info


def get_pending_intent(apkObj):
    """find if an apk object contains an pending Intent"""
    invoke=invokes(apkObj)
    pendingIntentCpt = 0
    for elem in invoke:
        if elem.find("Landroid/app/PendingIntent;") != -1:
            pendingIntentCpt += 1

    return pendingIntentCpt

def get_all_infos(apkObj):
    """this function is used to retrieve all information from an apk file as a set"""
    return{
        'header':header(apkObj),
        'classes':list(set(classes(apkObj))),
        'methods':list(set(methods(apkObj))),
        #'fields':fields(apkObj),
        'strings':functions.normalzation_of_strings(list(set(strings(apkObj))),classes(apkObj),list(set(methods(apkObj)))),
        'invokes':list(set(invokes(apkObj))),
        'branch_opcodes' : list(set(apkObj.dvm.get_BRANCH_DVM_OPCODES())),
        'apiVersion' : apkObj.dvm.get_api_version(),
        'len_methods': apkObj.dvm.get_len_methods(),
        'is_debug':is_debug(apkObj),
        'urls':list(set(get_urls(apkObj))),
        'suspicious_api':get_suspicious_api_calls(apkObj),
        'external_classes':external_classes(apkObj),
        'load_methods':get_load_methods(apkObj),
        'load_classes':get_load_classes(apkObj),
        'icc_methods':get_icc_methods(apkObj),
        'pending_intent':get_pending_intent(apkObj),
        'reflection':get_reflected_api(apkObj)



    }






