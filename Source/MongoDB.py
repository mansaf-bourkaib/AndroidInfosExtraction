from pymongo import MongoClient
import Environment
import json
import os


"""This module allows you to save the extraction result in the MongoDB database"""

def read_json_file(path):
    with open(path,'r') as fp:
        data=json.load(fp)
    return data

def insert_data_into_db(data):
    ip_server=urllib.quote_plus(Environment.MONGODB_SERVER_IP)
    user= urllib.quote_plus(Environment.MONGODB_USER_NAME)
    password=urllib.quote_plus(Environment.MONGODB_USER_PASSWORD)
    uri = "mongodb://"+user+":"+password+"@"+ip_server+"/?authSource=apkinfo"

    mydb1 = MongoClient(uri).apkinfo
    mycol1 = mydb1[Environment.MONGODB_COLLECTION_NAME]

    x = mycol1.insert_one({'_id': data['APK']['sha256'], 'APK': data['APK'],
                           'DVM': {
                               'header': data['DVM']['header'],
                               'classes': data['DVM']['classes'],
                               'methods': data['DVM']['methods'],
                               'strings': data['DVM']['strings'],
                               'invokes': data['DVM']['invokes'],
                               'branch_opcodes': data['DVM']['branch_opcodes'],
                               'apiVersion': data['DVM']['apiVersion'],
                               'len_methods': data['DVM']['len_methods'],
                               'is_debug': data['DVM']['is_debug'],
                               'urls': data['DVM']['urls'],
                               'suspicious_api': data['DVM']['suspicious_api'],
                               'external_classes': data['DVM']['external_classes'],
                               'load_methods': data['DVM']['load_methods'],
                               'load_classes': data['DVM']['load_classes'],
                               'icc_methods': data['DVM']['icc_methods'],
                               'pending_intent': data['DVM']['pending_intent'],
                               'reflection': data['DVM']['reflection']
                           }, 'DEX': data['DEX'], 'Virus_Total': data['Virus_Total']})




