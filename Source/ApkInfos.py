from copy import deepcopy
from collections import Counter
import datetime
import os
import magic
import re
import functions

"""This module allows you to extract information from the APK object"""

def find_files(apkObj, with_magic=False):
    """Find all the files contained in an APK object."""

    for name in apkObj.apk.zip.namelist():
        file = apkObj.apk.zip.open(name)
        info = apkObj.apk.zip.getinfo(name)


        if not with_magic:
            yield file, info
        else:
            mag = magic.from_buffer(file.read(1024))
                # cannot use seek(0) on zip files
                # we have to re-open the file
            file = apkObj.apk.zip.open(name)
            yield file, info, mag

def get_files_infos(apkObJ):
    """Extract infos from each file in an APK object."""
    files_list=[]

    for file,info,mag in find_files(apkObJ,True):
        inf={}
        inf["filename"]=info.filename
        inf["mode"]=file.mode
        inf["file_size"]=info.file_size
        #inf["magic"]=mag
        inf["sha256"]=functions.sha256(file.read())
        date_time = info.date_time
        if date_time[1] == 0:
            date_time = list(date_time)
            date_time[1] = 1
            date_time = tuple(date_time)
        if date_time[2] == 0:
            date_time = list(date_time)
            date_time[2] = 1
            date_time = tuple(date_time)
        inf["date_time"]=datetime.datetime(*date_time[0:6]).strftime("%Y-%m-%d %H:%M:%S")
        files_list.append(inf)

    return files_list

def get_files_extension(apkObJ):
    """find all existing file extensions in an APK file"""
    files_extension = []
    for file, info, mag in find_files(apkObJ, True):
        files_extension.append(os.path.splitext(info.filename)[1])

    extensions_counter=Counter(files_extension)
    extensions_dict={}
    for key, value in extensions_counter.items():
        extensions_dict[key]=value

    return extensions_dict



def get_cert_infos(apkObj):
    """Find all certificates contained in an APK object."""
    certificats=[]
    for cert in apkObj.apk.get_certificates():
        info={}
        info["sha1"] = cert.sha1.hex()
        info["sha256"] = cert.sha256.hex()
        info["cert_issuer"] = cert.issuer.human_friendly
        info["cert_subject"] = cert.subject.human_friendly
        info["cert_hash_algo"] = cert.hash_algo
        info["cert_sig"] = cert.signature_algo
        info["cert_num"] = str(cert.serial_number)
        certificats.append(info)
    return certificats

def find_natives(apkObj):
    """Find only native files contained in an APK object."""
    natives_code=[]
    ELF_MAGIC_PATTERN = re.compile(r"ELF", re.IGNORECASE)
    for file, info, magic in find_files(apkObj, with_magic=True):
        if ELF_MAGIC_PATTERN.match(magic):
            inf = {}
            inf["filename"] = info.filename
            inf["file_size"] = info.file_size
            inf["magic"] = magic
            inf["sha256"] = functions.sha256(file.read())
            inf["date_time"] = info.date_time
            natives_code.append(inf)
    return natives_code

def find_intent_filter(apkObj):
    """Find only Intent filter contained in an APK object."""
    intent_filter = {'action': [], 'category': []}
    for act in apkObj.apk.get_activities():
        inte = apkObj.apk.get_intent_filters('activity', act)
        if bool(inte) == True:
            for action in inte['action']:
                if action not in intent_filter['action']:
                    intent_filter['action'].append(action)
            if 'category' in inte:
                for category in inte['category']:
                    if category not in intent_filter['category']:
                        intent_filter['category'].append(category)
    for srv in apkObj.apk.get_services():
        inte = apkObj.apk.get_intent_filters('service', srv)
        if bool(inte) == True:
            for action in inte['action']:
                if action not in intent_filter['action']:
                    intent_filter['action'].append(action)
            if 'category' in inte:
                for category in inte['category']:
                    if category not in intent_filter['category']:
                        intent_filter['category'].append(category)

    for rcv in apkObj.apk.get_receivers():
        inte = apkObj.apk.get_intent_filters('service', rcv)
        if bool(inte) == True:
            for action in inte['action']:
                if action not in intent_filter['action']:
                    intent_filter['action'].append(action)
            if 'category' in inte:
                for category in inte['category']:
                    if category not in intent_filter['category']:
                        intent_filter['category'].append(category)

    return intent_filter


def find_ressources(apkObj):
    """Find only Resources informations contained in an APK object."""
    res=apkObj.apk.get_android_resources()
    res._analyse()
    values = deepcopy(res.values)

    # change the key of default lang to ASCII
    for package, languages in values.items():
        for lang in languages.keys():
            if lang == "\x00\x00":
                languages["DEFAULT"] = languages.pop(lang)

    return values


def get_all_infos(apkObj):
    """this function is used to retrieve all information from an apk file as a set"""
    return {
        
        'appID' : apkObj.apk.get_package(),
        'appName' : apkObj.apk.get_app_name(),
        'sha256': functions.sha256(apkObj.apk.get_raw()),
        'sha512': functions.sha512(apkObj.apk.get_raw()),
        'md5': functions.md5(apkObj.apk.get_raw()),
        'codeVersion' : apkObj.apk.get_androidversion_name(),
        'sdkVersion' : apkObj.apk.get_target_sdk_version(),
        'minSdk': apkObj.apk.get_min_sdk_version(),
        'permissions' : functions.split_permissions(apkObj.apk.get_permissions()),
        'third_party_permissions' : apkObj.apk.get_requested_third_party_permissions(),
        'hardware_components' : apkObj.apk.get_features(),
        'activities' : apkObj.apk.get_activities(),
        'services' : apkObj.apk.get_services(),
        'receivers' : apkObj.apk.get_receivers(),
        'providers' : apkObj.apk.get_providers(),
        'libraries' : apkObj.apk.get_libraries(),
        'intentFilter': find_intent_filter(apkObj),
        'mainActivity' : apkObj.apk.get_main_activity(),
        'apk_sig_files' : apkObj.apk.get_signature_names(),
        'apk_is_signed' : apkObj.apk.is_signed(),
        'apk_is_valid' : apkObj.apk.is_valid_APK(),
        'files':get_files_infos(apkObj),
        'files_extension': functions.split_extension_file(get_files_extension(apkObj)),
        'certificats':get_cert_infos(apkObj)


    }