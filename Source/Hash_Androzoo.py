import Environment
import random
import pandas as pd

def get_rendom_hash(size):
    """Extract N (size) random hash existing in Androzoo"""

    nb_lines=98 #Number of lines in androzoo files

    idx = random.sample(range(1, nb_lines), size)

    #create skip index
    skip_idx=list(range(1,nb_lines))
    for index in idx:
        skip_idx.remove(index)

    df = pd.read_csv(Environment.HASH_LIST_FILE, delimiter = ',', skiprows=skip_idx)
    list_apk_hash256=list()
    for ind in df.index:
        info={'sha256': df['sha256'][ind],'vt_detection':df['vt_detection'][ind],'vt_scan_date':df['vt_scan_date']}
        list_apk_hash256.append(info)
    return list_apk_hash256