import lief
import json
import functions
import os
import Environment

"""this module allows to extract information from a DEX object"""

def get_all_infos(path):
    """Find all information  contained in an  Dex file."""
    #unzip Apk file
    hash_file=functions.sha256_file(path)
    extract_dir=Environment.DEX_DIR+hash_file
    os.mkdir(extract_dir)
    functions.unzipApk(path,extract_dir)
    dex_file=extract_dir+Environment.DEX_FILE_NAME

    lie = lief.DEX.parse(dex_file)
    jLie = lief.to_json(lie.header)
    jLie = json.loads(jLie)

    info={
        "dex_version":lief.DEX.version(dex_file),
        "dex_checksum" : jLie['checksum'],
        "dex_file_size" : jLie["file_size"],
        "dex_header_size" : jLie["header_size"],
        "dex_Magic" : functions.convert_to_assci(jLie["magic"]),
        "dex_sig" : functions.convert_signature(jLie['signature']),
        "dex_endian_tag" : hex(jLie['endian_tag'])
    }
    functions.remove_dir(extract_dir)
    return info