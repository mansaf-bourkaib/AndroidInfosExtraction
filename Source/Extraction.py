import Hash_Androzoo
import Androzoo
from APK import APK
import Environment
import ApkInfos
import DvmInfos
import VirusTotal
import DexInfos
import functions
import Redis_function
import MongoDB

"""This module takes in entering the path to the APK file or a sha256 hash, then it will download this file
 from Androzoo and extract this information and store it in the MongoDB database"""


def extract_info_random_apk(size):
    """This function allows to extract randomly N applications from Androzooo"""

    #1 chose randomly N APK hash from Androzoo file

    sha256_apk_list=Hash_Androzoo.get_rendom_hash(size)

    #sha256_apk_list=test.extract_50000_new_app(size)

    #2 Download and Extract information from each APK
    for elem in sha256_apk_list:
        apk_hash=elem["sha256"]
        Androzoo.download_apk(apk_hash)

        apk_path=Environment.DOWNLOAD_APK_DIR+apk_hash+".apk"
        apkObj=APK(apk_path)

        apk_infos=ApkInfos.get_all_infos(apkObj)
        dvm_infos=DvmInfos.get_all_infos(apkObj)
        vt_infos={'vt_detection':float(str(elem["vt_detection"])),'vt_scan_date':str(elem['vt_scan_date'][0])}
        dx_infos=DexInfos.get_all_infos(apk_path)

        result={"APK":apk_infos,"DVM":dvm_infos,"DEX":dx_infos,"Virus_Total":vt_infos}
        json_path = Environment.JSON_DIR + apk_hash + ".json"

    # 3 Save result in compresed json file
        functions.save_as_json(result,json_path)
        functions.compress_file(json_path)
        functions.remove_file(json_path)
        functions.remove_file(apk_path)
        return result


def extract_info(apk_path):
    """This function allows you to extract the information from an APK file"""
    apkObj = APK(apk_path)

    #Extract info from APK object
    apk_infos = ApkInfos.get_all_infos(apkObj)
    dvm_infos = DvmInfos.get_all_infos(apkObj)
    vt_infos = VirusTotal.get_virus_total_infos(functions.sha256_file(apk_path))
    dx_infos = DexInfos.get_all_infos(apk_path)

    result = {"APK": apk_infos, "DVM": dvm_infos, "DEX": dx_infos, "Virus_Total": vt_infos}
    json_path = Environment.JSON_DIR + functions.sha256_file(apk_path) + ".json"

    # Save result in compressed  json file
    functions.save_as_json(result,json_path)


def extract_apk_info_from_redis():
    """This function allows to extract the information from the sha256
    which is found in the Redis database"""

    # 1 chose an apk sha256 from redis
    androzoo_info=Redis_function.read_from_redis(1)
    while len(androzoo_info) > 0:
    # 2 Download and Extract information from each APK
        androzoo_info=androzoo_info[0].decode("utf-8")
        androzoo_info=androzoo_info.split(",")
        apk_hash = androzoo_info[0]
        Androzoo.download_apk(apk_hash)

        apk_path = Environment.DOWNLOAD_APK_DIR + apk_hash + ".apk"
        apkObj = APK(apk_path)

        apk_infos = ApkInfos.get_all_infos(apkObj)
        dvm_infos = DvmInfos.get_all_infos(apkObj)
        try:
            score=int(androzoo_info[1])

            vt_infos = {'vt_detection': score, 'vt_scan_date': str(androzoo_info[2])}
            dx_infos = DexInfos.get_all_infos(apk_path)

            result = {"APK": apk_infos, "DVM": dvm_infos, "DEX": dx_infos, "Virus_Total": vt_infos}

            #save information in MongoDB
            MongoDB.insert_data_into_db(result)


            # 3 Save result in compresed json file
            # json_path = Environment.JSON_DIR + apk_hash + ".json"
            #functions.save_as_json(result, json_path)
            #functions.compress_file(json_path)
            #functions.remove_file(json_path)
            #functions.remove_file(apk_path)

            androzoo_info = Redis_function.read_from_redis(1)
        except ValueError:
            androzoo_info = Redis_function.read_from_redis(1)
            continue




