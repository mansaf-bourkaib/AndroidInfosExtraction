import hashlib
from zipfile import ZipFile
import os
import gzip
import shutil
import json
import random
import pandas as pd
import Environment


"""This module groups together all the functions used in the project"""

"""SHA 256 HASH """
def sha256(raw):
    Sha256 = hashlib.sha256()
    Sha256.update(raw)
    Sha256=Sha256.hexdigest()
    return Sha256

"""SHA 512 HASH """
def sha512(raw):
    Sha512 = hashlib.sha512()
    Sha512.update(raw)
    Sha512 = Sha512.hexdigest()
    return Sha512

"""MD5 HASH """
def md5(raw):
    Md5 = hashlib.md5()
    Md5.update(raw)
    Md5 = Md5.hexdigest()
    return Md5

"""Function to unzip an APK archive """
def unzipApk(apkPath,dst_path):
    with ZipFile(apkPath, 'r') as zip:
        zip.extractall(dst_path)

"""Function to compress a file"""
def  compress_file(path):
    with open(path, 'rb') as f_in:
        with gzip.open(path+".gz", 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)

"""Function to delete File"""
def remove_file(path):
    os.remove(path)

"""This function will receive data in the form of a dictionary and save it in a JSON file"""
def save_as_json(data,path):
    json_dump = json.dumps(data)
    with open(path, "w") as outfile:
        outfile.write(json_dump)

"""This function allows the computation of sha256 hash of a file"""
def sha256_file(file_path):
    with open(file_path, "rb") as f:
        bytes = f.read()  # read entire file as bytes
        readable_hash = hashlib.sha256(bytes).hexdigest();
    return readable_hash

def remove_dir(path):
    shutil.rmtree(path)


"""This function is created to extract the SHA256 of N application from the Androzoo CSV file"""
def select_random_app(size):

    #nb_lines=15063587 #Number of lines in androzoo files
    nb_lines=98 #Number of lines in androzoo file (change it by the exact number of applications in androzoo)

    idx = random.sample(range(1, nb_lines), size)

    #create skip index
    skip_idx=list(range(1,nb_lines))
    for index in idx:
        skip_idx.remove(index)

    df = pd.read_csv(Environment.HASH_LIST_FILE, delimiter = ',', skiprows=skip_idx,encoding='UTF-8')

    list_apk_hash256=list()

    for ind in df.index:

        if str(df['vt_detection'][ind]) != 'nan' and str(df['vt_scan_date'][ind]):
            info = str(df['sha256'][ind]) + "," + str(int(df['vt_detection'][ind])) + "," + str(df['vt_scan_date'][0])
            list_apk_hash256.append(info)
    return list_apk_hash256


def split_permissions(permissions):
    new_permission = list()
    for p in permissions:
        p = p.split(".")
        p = p[len(p) - 1]
        new_permission.append(p)
    return new_permission


def split_extension_file(ext):
    new_extension_list=list()
    for extension in ext:
        extension=extension.split('.')
        if len(extension) > 1:
            new_extension_list.append(extension[1])
        else:
            new_extension_list.append("")
    return new_extension_list


def convert_to_assci(hexa):
    str=""
    for val in hexa:
        str=str+chr(val)
    return str


def convert_signature(sig):
    signature = ""
    for val in sig:
        signature += hex(val)[2:4]
    return signature


def normalzation_of_strings(strings,classes,methods):
    new_str_list=list()
    for elem in strings:
        if elem not in classes and elem not in methods:
            new_str_list.append(elem)
    return new_str_list