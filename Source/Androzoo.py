import urllib.request
import Environment



def download_apk(sha256):
    """Download APK from Androzoo and save it in download dir."""

    download_dir=Environment.DOWNLOAD_APK_DIR
    download_url=Environment.ANDROZOO_DOWNLOAD_URL

    params = {"apikey": Environment.ANDROZOO_APIKEY, "sha256": sha256}
    url=download_url+"apikey="+params["apikey"]+"&sha256="+params["sha256"]

    urllib.request.urlretrieve(url, download_dir+sha256+".apk")


