import redis
import Environment
import functions


"""This module is created to store the sha256 hashes of the applications
 that we want to extract their information in the redis data base"""

def upload_to_redis(list_hash):
    r = redis.Redis(host=Environment.REDIS_SERVER, port=Environment.REDIS_PORT,password=Environment.REDIS_PASSWORD)
    for elem in list_hash:
            r.sadd(Environment.REDIS_BD_NAME, elem)

def read_from_redis(size):
    r = redis.Redis(host=Environment.REDIS_SERVER, port=Environment.REDIS_PORT, password=Environment.REDIS_PASSWORD)
    return r.spop(Environment.REDIS_BD_NAME,count=size)

def initialisation_of_redisDB(nb_elements):
    r = redis.Redis(host=Environment.REDIS_SERVER, port=Environment.REDIS_PORT, password=Environment.REDIS_PASSWORD)
    androzoo_list=functions.select_random_app(nb_elements)
    for elem in androzoo_list:
        r.sadd(Environment.REDIS_BD_NAME,elem)




if __name__ == "__main__":
    if len(sys.argv) ==2:
       nb_element=int(sys.argv[1]) #Number of hash has uploded in redisDB
       initialisation_of_redisDB(nb_element)
       print("Generate and upload "+str(nb_element)+" Apk androzoo informations in redis DB .")




