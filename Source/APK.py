from androguard.core.bytecodes import apk as AndroAPK
from androguard.core.bytecodes import dvm as AndroDVM
from androguard.misc import AnalyzeAPK

class APK(object):
    def __init__(self, apkfile, path=True):
        """Create an object from an APK file.
        path=False: file content, path=True: file path."""
        self.apk = AndroAPK.APK(apkfile)
        self.a,self.d,self.dx=AnalyzeAPK(apkfile)
        self.dex= self.apk.get_dex()
        self.dvm =  AndroDVM.DalvikVMFormat(self.dex)



