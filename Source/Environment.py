import os
import platform


"""This file contains all the information about the extarctor runtime environment"""

#Virus Total Informations

VIRUSTOTAL_APIKEY = "API_key"

#Androzoo Informations
ENV_ANDROZOO_APIKEY = "ANDROZOO_APIKEY"
ANDROZOO_APIKEY ="API KEY"
ENV_ANDROZOO_DOWNLOAD_URL = "https://androzoo.uni.lu/api/download?"
ANDROZOO_DOWNLOAD_URL = "https://androzoo.uni.lu/api/download?"

if platform.system()=='Windows':
    #Application download directory
    DOWNLOAD_APK_DIR=os.getcwd()+"\\tmp\\apk\\"

    #The path to the file which contains information on all existing applications in androzoo
    HASH_LIST_FILE=os.getcwd()+"\\hash\\latest.csv"

    #The json output storage directory
    JSON_DIR=os.getcwd()+"\\jsonFiles\\"

    #Dex Informations
    DEX_DIR=os.getcwd()+"\\tmp\\dex\\"
    DEX_FILE_NAME="\\classes.dex"
elif platform.system()=='Linux':
    #Application download directory
    DOWNLOAD_APK_DIR=os.getcwd()+"/tmp/apk/"

    #The path to the file which contains information on all existing applications in androzoo
    HASH_LIST_FILE=os.getcwd()+"/hash/latest.csv"

    #The json output storage directory
    JSON_DIR=os.getcwd()+"/jsonFiles/"

    #Dex Informations
    DEX_DIR=os.getcwd()+"/tmp/dex/"
    DEX_FILE_NAME="/classes.dex"


#Redis configuration
REDIS_SERVER='IP Address'
REDIS_PORT='Port Number'
REDIS_PASSWORD="Password"
REDIS_BD_NAME="Database Name"


#MongoDb Configuration
MONGODB_SERVER_IP="'IP Address'"
MONGODB_SERVER_PORT="Port Number"
MONGODB_DATABASE_NAME="apkinfo"
MONGODB_COLLECTION_NAME='Data'
MONGODB_USER_NAME="User name"
MONGODB_USER_PASSWORD="Password"





